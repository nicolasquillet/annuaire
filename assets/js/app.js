/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.css';
import 'bootstrap';

// Import jQuery
import $ from 'jquery';

// Import Phone input library
import intlTelInput from 'intl-tel-input/build/js/intlTelInput';
import 'intl-tel-input/build/js/utils';

// Initalize all phone inputs with bootstrap
var phones = document.querySelectorAll(".input-phone");
var itis = [];

phones.forEach(function(item){
    intlTelInput(item, {
        initialCountry: 'fr',
        preferredCountries: ['fr','be','es','de','it'],
        separateDialCode:true,
        utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.11/js/intlTelInput.min.js',
        customContainer: 'form-control p-0 border-0',
        hiddenInput: "number"
    });
});
