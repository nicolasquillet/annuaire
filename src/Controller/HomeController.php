<?php

namespace App\Controller;

use App\Entity\Phone;
use App\Form\PhoneType;
use App\Manager\PhoneManager;
use App\Repository\PhoneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @property PhoneManager phoneManager
 * @Route("/")
 */
class HomeController extends AbstractController
{

    public function __construct(PhoneManager $phoneManager)
    {
        // Todo: create and use a custom service manager in order to load multiples manager at once.
        $this->phoneManager = $phoneManager;
    }

    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $phones = [];

        $search = $request->get('number', null);

        if(!empty($search)) {
            $phones = $this->phoneManager->searchByNumber($search);
        }

        return $this->render('index.html.twig', [
            'phones' => $phones,
            'search' => $search
        ]);
    }
}
