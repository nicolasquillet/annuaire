# Official Docker Images
FROM  php:7.4.2-apache

# Update & Upgrade OS
RUN apt-get update
RUN apt upgrade -y

# GIT
RUN apt-get install -y git

# ZIP
RUN apt-get install -y libzip-dev
RUN docker-php-ext-install zip

# PDO PHP Extension
RUN docker-php-ext-install pdo

# Install the PHP pdo_mysql extention
RUN docker-php-ext-install pdo_mysql

# XML PHP Extension
RUN apt-get install -y libxml++2.6-dev
RUN docker-php-ext-install xml

# Symfony recommanded extensions
RUN docker-php-ext-install pcntl ctype iconv json  session simplexml tokenizer

# Install composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Install nodeJs
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash -
RUN apt-get install -y nodejs

# Install YARN
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get install -y apt-transport-https ca-certificates
RUN apt update && apt install yarn
RUN echo yarn --version

# Apache
RUN a2enmod rewrite

# DocumentRoot
RUN sed -i  's|DocumentRoot /var/www/html|DocumentRoot /var/www/html/public|g' /etc/apache2/sites-available/000-default.conf
