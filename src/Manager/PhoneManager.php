<?php

namespace App\Manager;

use App\Entity\Phone;
use App\Entity\User;
use App\Repository\PhoneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @property PhoneRepository phones
 * @property AuthorizationCheckerInterface mcd
 * @property EntityManagerInterface em
 */
class PhoneManager
{

    /**
     * PhoneManager constructor.
     * @param EntityManagerInterface $manager
     * @param PhoneRepository $phoneRepository
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(EntityManagerInterface $manager, PhoneRepository $phoneRepository, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $manager;
        $this->mcd = $authorizationChecker;
        $this->phones = $phoneRepository;
    }

    public function getRepository()
    {
        return $this->phones;
    }

    /**
     * @param Phone $phone
     * @return Phone
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     */
    public function updatePhone(Phone $phone): Phone
    {
        // Check permissions
        if (!$this->mcd->isGranted('update', $phone)) {
            throw new AccessDeniedException();
        }

        $this->em->flush();

        // DO SOME OTHERS FEATURES LIKE FIRE EVENT OR SEND EMAIL

        return $phone;
    }

    /**
     * @param Phone $phone
     * @return Phone
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     */
    public function createPhone(Phone $phone): Phone
    {
        // Check permissions
        $this->mcd->isGranted('create', Phone::class);

        $this->em->persist($phone);
        $this->em->flush();

        // DO SOME OTHERS FEATURES LIKE FIRE EVENT OR SEND EMAIL

        return $phone;
    }

    /**
     * @param Phone $phone
     * @return void
     * @throws \Symfony\Component\Finder\Exception\AccessDeniedException
     */
    public function deletePhone(Phone $phone): void
    {
        // Check permissions
        if (!$this->mcd->isGranted('delete', $phone)) {
            throw new AccessDeniedException();
        }

        $this->em->remove($phone);
        $this->em->flush();

        // DO SOME OTHERS FEATURES LIKE FIRE EVENT OR SEND EMAIL
    }
    public function searchByNumber(string $number): array
    {
        return $this->getRepository()->findByNumberLike($number);
    }
}