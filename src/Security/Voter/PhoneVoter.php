<?php

namespace App\Security\Voter;

use App\Entity\Phone;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class PhoneVoter extends Voter
{

    const CREATE = 'create';
    const READ = 'read';
    const UPDATE = 'update';
    const DELETE = 'delete';

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
                self::CREATE,
                self::READ,
                self::UPDATE,
                self::DELETE,
            ])
            && $subject instanceof Phone;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            $user = null;
        }

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($subject, $user);
            case self::READ:
                return $this->canRead($subject, $user);
            case self::UPDATE:
                return $this->canUpdate($subject, $user);
            case self::DELETE:
                return $this->canDelete($subject, $user);
        }

        return false;
    }

    public function canCreate(Phone $phone, User $user = null)
    {
        // Only authenticated users can create a Phone entry
        return $user instanceof User;
    }

    public function canRead(Phone $phone, User $user = null)
    {
        // Everybody can read a phone entry
        return true;
    }

    public function canUpdate(Phone $phone, User $user = null)
    {
        // Only the Author can update a Phone entry

        return $user instanceof User && $user === $phone->getUser();
    }

    public function canDelete(Phone $phone, User $user = null)
    {
        // If user can update, he can delete.
        return $this->canUpdate($phone, $user);
    }

}
