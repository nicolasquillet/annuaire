<?php

namespace App\Controller;

use App\Entity\Phone;
use App\Form\PhoneType;
use App\Manager\PhoneManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @property PhoneManager phoneManager
 * @Route("/phone")
 */
class PhoneController extends AbstractController
{
    public function __construct(PhoneManager $phoneManager)
    {
        // Todo: create and use a custom service manager in order to load multiples manager at once.
        $this->phoneManager = $phoneManager;
    }

    /**
     * @Route("/new", name="phone_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        // Initiate new Phone
        $phone = new Phone();

        // Check permissions directly from controller
        $this->denyAccessUnlessGranted('create', $phone);

        // Initiate form and handle request if needed
        $form = $this->createForm(PhoneType::class, $phone);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $phone->setUser($this->getUser());
            $this->phoneManager->createPhone($phone);
            return $this->redirectToRoute('phone_index');
        }

        return $this->render('phone/new.html.twig', [
            'phone' => $phone,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="phone_show", methods={"GET"})
     */
    public function show(Phone $phone): Response
    {
        // Check permissions directly from controller
        $this->denyAccessUnlessGranted('read', $phone);

        return $this->render('phone/show.html.twig', [
            'phone' => $phone,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="phone_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Phone $phone): Response
    {
        $form = $this->createForm(PhoneType::class, $phone);
        $form->handleRequest($request);

        // Check permissions directly from controller
        $this->denyAccessUnlessGranted('update', $phone);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->phoneManager->updatePhone($phone);

            return $this->redirectToRoute('phone_index');
        }

        return $this->render('phone/edit.html.twig', [
            'phone' => $phone,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="phone_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Phone $phone): Response
    {
        if ($this->isCsrfTokenValid('delete' . $phone->getId(), $request->request->get('_token'))) {
            $this->phoneManager->deletePhone($phone);
        }

        return $this->redirectToRoute('phone_index');
    }
}
