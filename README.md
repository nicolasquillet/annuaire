# Annuaire

## Contexte
Le choix s'est porté sur le framework Symfony 4 afin de prouver mon adaptabilité sur un framework que je connais peu.
Concernant la base de donnée, une base MariaDB avec des tables en InnoDB afin de pouvoir gérer le relationnel. 

## Installation
Récupération du dépot Git
```
git clone git@gitlab.com:nicolasquillet/annuaire.git
```

Monter les containers nécessaires, application web exposée sur le http://localhost:8082
> (Optionnel) Ajoutez le domaine annuaire.local pointant vers localhost dans le fichier hosts de votre OS afin d'utiliser le nom de domaine et le reverse proxy nginx
```
cd annuaire && docker-compose up -f
```
> Vous devrez peut-être mettre à jour les chemins des volumes dans le `docker-compose.yml` afin de coller à votre OS, a terme une gestion différente des volumes est à prévoir.

Copie du fichier d'environnement
```
cp .env.example .env
```

Installation des dépendances PHP
```
docker exec annuaire composer install --no-dev
```

Exécution des migrations afin de construire la base de donnée.
```
docker exec -it annuaire bin/console doctrine:migrations:migrate
```

## Principes et fonctionnalités

- Migrations (via Doctrine)
- Permissions d'utilisateur (via Access Decision Manager)
- Routes définies via Annotations
- Login / Register basique de Symfony
- Utilisation d'une couche Manager pour factoriser les actions métiers
- Multilangue prêt
- Intégration de webpack pour gestion dépendances frontend & automatisation SASS & ES6
- Dockerfile & Docker-Compose

## Reste à faire

- Meilleure validation de numéro de téléphone (Regex et/ou API de validation)
- Gestion du multilangue des labels de champs de formulaire.
- Gérer plutôt des permissions par rôles d'utilisateur plutôt que par loggued/guest
- Vérification de la validité de l'email (API et/ou Regex et/ou check MX manuel et/ou Double Optin)
- CI/CD
- Tests unitaire
